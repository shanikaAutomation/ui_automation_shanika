package com.sysco.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class ElementsPage extends PageBase {

    WebDriverWait wait = new WebDriverWait(driver, 20);

    private By clickableIcon = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[5]/div/div[1]/a/span/span");
    private By rdBtnFemale = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[7]/div/div/div/form/input[2]");
    private By chkBike = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[8]/div/div/div/form/input[1]");
    private By chkCar = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[8]/div/div/div/form/input[2]");
    private By carDropDown = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[9]/div/div/div/select");
    private By drpDwnSaabOption = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[9]/div/div/div/select/option[2]");
    private By drpDwnVehicles = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[9]/div/div/div/select");
    private By tabOption = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[10]/ul/li[2]/a");
    private String tblData = "//*[@id=\"et-boc\"]/div/div[3]/div/div[2]/div[5]/div/table/tbody/tr[index1]/td[index2]";
    private By tabSelection = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[10]/div/div[2]/div");


    public static void sleep(int i) {
    }

    public void clickClickableIcon() {
        driver.findElement(clickableIcon).click();
    }

    public void clickFemaleRadioButton() {
        driver.findElement(rdBtnFemale).click();
    }

    public void selectBikeCheckBox() {
        driver.findElement(chkBike).click();
    }

    public void selectCarCheckBox() {
        driver.findElement(chkCar).click();
    }

    public void clickCarDropDown() {
        driver.findElement(carDropDown).click();

    }

    public void clickSaabOption() {
        driver.findElement(drpDwnSaabOption).click();

    }

    public boolean isSaabOptionDisplayed() {
        return driver.findElement(drpDwnVehicles).isDisplayed();
    }

    public void clickTabOption() {

        driver.findElement(tabOption).click();
        waitUntillElementVisible(tabSelection);
    }

    public boolean isSelectedFemaleRadioBtn() {
        return driver.findElement(rdBtnFemale).isSelected();
    }

    public boolean isCheckedCarCheckBox() {
        return driver.findElement(chkCar).isSelected();
    }

    public boolean isCheckedBikeCheckBox() {
        return driver.findElement(chkBike).isSelected();
    }

    public String getSelectedTabText(){

       return driver.findElement(tabSelection).getText();

        //WebElement tabSelectionTextElement = driver.findElement(tabSelection);
        //String tabSelectionText = tabSelectionTextElement.getText();
        //return tabSelectionText;


    }

    public String getTableData(String index1, String index2){
        WebElement tblDataSalary = driver.findElement(By.xpath(tblData.replace("index1",index1).replace("index2",index2)));
        String salaryText = tblDataSalary.getText();
        return salaryText;

    }



}
