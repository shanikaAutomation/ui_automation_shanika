package com.sysco.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SuccessPage extends PageBase {
    private By successMessageText = By.className("entry-title");

    public String getSuccessMessageText() {
        WebElement successMessageElement = driver.findElement(successMessageText);
        String successMessage = successMessageElement.getText();
        return successMessage;

        //return driver.findElement(successMessageText).getText();
    }

}

