package com.sysco.pages;

import org.openqa.selenium.*;
import com.sysco.utils.WebDriverManager;
import org.openqa.selenium.support.ui.*;
import org.openqa.selenium.By;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;


public class PageBase {

    protected WebDriver driver;
    private Wait wait;

    public PageBase(){

        driver = WebDriverManager.getDriver();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public void waitUntillElementClickable(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(element));

    }

    public void waitUntillElementVisible(By by){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));

    }

    /*public WebElement waitForElementToBeClickable(By by) {
        wait = new FluentWait<>(driver)
                .withTimeout(20, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MICROSECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementNotVisibleException.class);

        wait.until(new Function() {
            @Override
            public WebElement apply(Object o) {
                if (driver.findElement(by).getAttribute("value").equals("success")) {
                    return driver.findElement(by);
                }
                return null;
            }
        });
    }*/

}
