package com.sysco.elements;

import com.sysco.pages.ElementsPage;
import com.sysco.pages.PageBase;
import com.sysco.pages.SuccessPage;
import com.sysco.pages.TestBase;
import com.syscolab.qe.core.reporting.SyscoLabListener;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

@Listeners(SyscoLabListener.class)
public class ElementsTest extends TestBase {

    private static final String LINK_TEXT_SUCCESS_MESSAGE = "Link success";
    private static final String Tab_Selection_Text = "Tab 2 content";


    @BeforeClass
    public void initClass(ITestContext iTestContext) {

        iTestContext.setAttribute("feature", "Assignment Shanikaw - Assignment Shanikaw");
    }

    @Test()
    public void testClickableIcon(){
        SoftAssert softAssert = new SoftAssert();
        ElementsPage page = new ElementsPage();
        page.clickClickableIcon();
        SuccessPage successPage = new SuccessPage();
        String actualLinkTextSuccessMessage =  successPage.getSuccessMessageText();
        softAssert.assertEquals(actualLinkTextSuccessMessage, LINK_TEXT_SUCCESS_MESSAGE,"Link text success message incorrect");
        softAssert.assertAll();
        driver.navigate().back();


    }

    @Test(dependsOnMethods = "testClickableIcon")
    public void testSelectRadioButton(){
        SoftAssert softAssert = new SoftAssert();
        ElementsPage page = new ElementsPage();
        page.clickFemaleRadioButton();
        softAssert.assertTrue(page.isSelectedFemaleRadioBtn(),"Female radio button did not select");
        softAssert.assertAll();

    }

    @Test(dependsOnMethods = "testSelectRadioButton")
    public void testSelectMultipleCheckBoxes(){
        SoftAssert softAssert = new SoftAssert();
        ElementsPage page = new ElementsPage();
        page.selectBikeCheckBox();
        page.selectCarCheckBox();
        softAssert.assertTrue(page.isCheckedCarCheckBox(),"Car check box did not select");
        softAssert.assertTrue(page.isCheckedBikeCheckBox(),"Bike check box did not select");
        softAssert.assertAll();

    }

    @Test(dependsOnMethods = "testSelectMultipleCheckBoxes")
    public void testDropDownValueSelection(){
        SoftAssert softAssert = new SoftAssert();
        ElementsPage page = new ElementsPage();
        page.clickCarDropDown();
        page.clickSaabOption();
        softAssert.assertTrue(page.isSaabOptionDisplayed(),"Saab Option did not select");
        softAssert.assertAll();

    }

    @Test(dependsOnMethods = "testDropDownValueSelection")
    public void testGetSalaryDetails(){
        SoftAssert softAssert = new SoftAssert();
        ElementsPage page = new ElementsPage();
        System.out.println(page.getTableData("3","3"));
        softAssert.assertEquals(page.getTableData("3","3"),"$120,000+");
        softAssert.assertAll();
    }

    @Test(dependsOnMethods = "testGetSalaryDetails")
    public void testSelectTab(){
        SoftAssert softAssert = new SoftAssert();
        ElementsPage page = new ElementsPage();
        page.clickTabOption();
        String tabSelectionTextMessage =  page.getSelectedTabText();
        softAssert.assertEquals(tabSelectionTextMessage, Tab_Selection_Text,"Tab Selection Text is incorrect");
        softAssert.assertAll();

    }

    private void sleep(int i) {
    }

}
